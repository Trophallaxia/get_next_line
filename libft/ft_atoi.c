/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 15:26:19 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 18:16:46 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(const char *str)
{
	int					i;
	unsigned long long	number;
	int					neg;

	i = 0;
	number = 0;
	neg = 1;
	while (str[i] && (str[i] == ' ' || (str[i] >= '\t' && str[i] <= '\r')))
		i++;
	neg = str[i] == '-' ? -1 : 1;
	(str[i] == '+' || str[i] == '-') ? i++ : 0;
	while (str[i] >= '0' && str[i] <= '9')
	{
		number = number * 10 + str[i++] - '0';
	}
	if (number > 9223372036854775807 && neg == 1)
		return (-1);
	if (number > 9223372036854775807 && neg == -1)
		return (0);
	return (neg * (int)number);
}
