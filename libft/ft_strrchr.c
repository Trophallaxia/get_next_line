/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 17:38:00 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 13:40:48 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrchr(const char *s, int c)
{
	char *ptr;
	char *temp;

	ptr = (void *)0;
	temp = (char*)s;
	if ((char)c == '\0')
	{
		while (*temp)
			temp++;
		ptr = temp;
	}
	while (*temp)
	{
		if (*temp == (char)c)
			ptr = temp;
		temp++;
	}
	return (ptr);
}
