/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 15:59:31 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/07 21:37:02 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(const char *s1, const char *s2)
{
	char *tp1;
	char *tp2;

	tp1 = (char*)s1;
	tp2 = (char*)s2;
	while (*tp1 && *tp1 == *tp2)
	{
		tp1++;
		tp2++;
	}
	return ((unsigned char)*tp1 - (unsigned char)*tp2);
}
