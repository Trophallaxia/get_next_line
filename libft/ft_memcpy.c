/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 18:02:16 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 12:53:54 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t		i;
	const char	*tp_src;
	char		*tp_dst;

	tp_src = src;
	tp_dst = dst;
	i = 0;
	while (i < n)
	{
		tp_dst[i] = tp_src[i];
		i++;
	}
	return (dst);
}
