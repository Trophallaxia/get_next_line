/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 14:03:21 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/07 21:58:22 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *dest, const char *src)
{
	char	*tp_src;
	char	*tp_dest;

	tp_src = (char*)src;
	tp_dest = dest;
	while (*tp_src)
		*tp_dest++ = *tp_src++;
	*tp_dest = 0;
	return (dest);
}
